#-------------------------------------------------------------------------------
# Name:        TALDOWN
# Purpose:     A script to automate the download and storage of the weekly NPR
#              show, This American Life, hosted by Ira Glass.
# Author:      Alexander
#
# Created:     05/11/2013
# Copyright:   (c) Alexander 2013
# Licence:
#-------------------------------------------------------------------------------
import urllib
import urllib2

url = "http://www.thisamericanlife.org"

def get_page(url):
    try:
        return urllib2.urlopen(url).read()
    except:
        return ""

page = get_page(url)
div = page.find('''<div class="slider this-week active"''')
page = page[div:]
action = page.find('''<li class="download">''')
page = page[action:]
action_end = page.find('''</li>''')
page = page[21:action_end]
download = page.find('''"http''')
download_end = page.find('''download''')
mp3 = page[download+1:download_end-2]
file_name = page[download_end-9:download_end-2]
print("Now downloading: This American Life epsiode " + file_name[:-4] + "...")
urllib.urlretrieve(mp3, file_name)

